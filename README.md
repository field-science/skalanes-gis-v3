# Skalanes GIS v3
New version of Skalanes GIS

Old Version: https://code.cs.earlham.edu/field-science/skalanes-gis

## Details
ISN93/Lambert EPSG:3057

## Organization Strategy
- `Project.qgz`
- `[CONTRIBUTOR]_[YEAR]/`
  - Assets


# Current Structure
- **`skalanes.qgz`** - v3 QGIS file, created with version 3.22.13
- **`README.md`** - This file.
- `Skalanes_2017/` - 2017 Skalanes Imagery provided by Oli
  - high res tif of the fjord + DSM
  - Assorted GIS files
- `EC-IFS-2021/` - Assemblies from survey flights conducted in 2021 by the Earlham IFS program.
  - 6/17/21 River Flight DSM+Orthophoto (Skydio)
  - 6/21/21 Roundhouse DSM+Orthophoto (P3)


# To be Added
- `EC-IFS-2019/` - Assemblies from survey flights conducted in 2021 by the Earlham IFS program. 
  - Arch 1 NIR Clache-Canny-v1 2019 (skalanes.qgz v1)
  - Arch 1 NIR Canny-v1 2019 (skalanes.qgz v1)
  - Arch 1 VLI 2019 (skalanes.qgz v1)
  - Arch 1 NIR DSM 2019 (skalanes.qgz v1)
- `Contributor / Year Unknown`
  - River Points - Elevation readings from Ytri-sanda Valley (skalanes.qgz v1), EC_IFS 2021
  - Tern Colony - Outline of Tern Colony (skalanes.qgz v1), Skalanes 2019
  - Skalanes_linur (skalanes.qgz v1) Icelandic Archaeology Ministry, report has date
  - Skalanes_punktar (skalanes.qgz v1) ""
  - Skalanes_svaedi (skalanes.qgz v1) ""
  - B/W Skalanes High Res tif (skalanes-first dan), Oli, ask about date
